"use strict";
//Evan Boren
var curr = 0;
var num1 = 0;
var num2 = 0;
/*var foo = function() { };
var bar = {};
var baz = { name: 'Eric', age: 35 };

baz.name;
baz['name'];

var foobar = [];
foobar.push({ name: 'Evan' });

foobar[0];
foobar.pop();
for (var i = 0; i < foobar.length; i++) {
    foobar[i];
}
*/
var action = null;
var results = null; //= document.getElementById("results").getElementsByTagName("li");
var dot = false;
var divisor = 10;
function setNum1(num) {
    if (!dot) {
        num1 = num1 * 10 + num;
    }
    else {
        num1 = num1 + (num / divisor);
        divisor = 10 * divisor;
    }
    console.log('num1 updated');
    if (results) {
        results.innerHTML = "<li>" + num1 + "</li>";
    }
}
function setNum2(num) {
    if (!dot) {
        num2 = num2 * 10 + num;
    }
    else {
        num2 = num2 + (num / divisor);
        divisor = 10 * divisor;
    }
    console.log('num2 updated');
    if (results) {
        results.innerHTML = "<li>" + num2 + "</li>";
    }
}
function setCurr(num) {
    curr = num;
    console.log('curr updated');
    if (results) {
        results.innerHTML = "<li>" + curr + "</li>";
    }
}
function numPress(num) {
    console.log('Called numPress with: ' + num);
    var theNum = num;
    return function () {
        //console.log(theNum);
        //console.log(one);
        //console.log(two);
        //console.log(three);
        if (action === null) {
            setNum1(theNum);
        }
        else if (action !== null) {
            setNum2(theNum);
        }
        console.log('numPress called');
    };
}
function add(x, y) {
    var result = 0;
    result = (x + y);
    console.log('add called');
    setCurr(result);
}
function sub(x, y) {
    var result = 0;
    result = (x - y);
    console.log('sub called');
    setCurr(result);
}
function mul(x, y) {
    var result = 0;
    result = (x * y);
    console.log('mul called');
    setCurr(result);
}
function div(x, y) {
    var result = 0;
    result = (x / y);
    console.log('div called');
    setCurr(result);
}
function clearCurr() {
    return function () {
        console.log('clearCurr called');
        setCurr(0);
        num1 = 0;
        num2 = 0;
        dot = false;
        if (results) {
            results.innerHTML = "<center><li>" + curr + "</li></center>";
        }
        action = null;
    };
}
function square(num) {
    console.log('square called');
    setCurr(num * num);
}
function squareRoot(num) {
    console.log('squareRoot called');
    setCurr(Math.sqrt(num));
}
function setAction(x) {
    var newAction = x;
    return function () {
        action = newAction;
        console.log('setAction Called');
        if (action === "square") {
            takeAction();
        }
        if (action === "squareRoot") {
            takeAction();
        }
        //if(action === "equ"){takeAction();}
        dot = false;
        divisor = 10;
    };
}
function setDot() {
    return function () {
        dot = true;
    };
}
function initialize() {
    curr = 0;
    num1 = 0;
    num2 = 0;
    action = null;
    divisor = 10;
    results = document.getElementById("results");
    var dot = false;
    var but1 = document.getElementById("but1");
    var but2 = document.getElementById("but2");
    var but3 = document.getElementById("but3");
    var but4 = document.getElementById("but4");
    var but5 = document.getElementById("but5");
    var but6 = document.getElementById("but6");
    var but7 = document.getElementById("but7");
    var but8 = document.getElementById("but8");
    var but9 = document.getElementById("but9");
    var but0 = document.getElementById("but0");
    var butAdd = document.getElementById("butAdd");
    var butMin = document.getElementById("butMin");
    var butDiv = document.getElementById("butDiv");
    var butMul = document.getElementById("butMul");
    var butSquare = document.getElementById("butSquare");
    var butSqrt = document.getElementById("butSqrt");
    var butEqu = document.getElementById("butEqu");
    var butClear = document.getElementById("butClear");
    var butDot = document.getElementById("butDot");
    if (but1) {
        but1.addEventListener("click", numPress(1));
    }
    if (but2) {
        but2.addEventListener("click", numPress(2));
    }
    if (but3) {
        but3.addEventListener("click", numPress(3));
    }
    if (but4) {
        but4.addEventListener("click", numPress(4));
    }
    if (but5) {
        but5.addEventListener("click", numPress(5));
    }
    if (but6) {
        but6.addEventListener("click", numPress(6));
    }
    if (but7) {
        but7.addEventListener("click", numPress(7));
    }
    if (but8) {
        but8.addEventListener("click", numPress(8));
    }
    if (but9) {
        but9.addEventListener("click", numPress(9));
    }
    if (but0) {
        but0.addEventListener("click", numPress(0));
    }
    if (butAdd) {
        butAdd.addEventListener("click", setAction("add"));
    }
    if (butMin) {
        butMin.addEventListener("click", setAction("sub"));
    }
    if (butMul) {
        butMul.addEventListener("click", setAction("mul"));
    }
    if (butDiv) {
        butDiv.addEventListener("click", setAction("div"));
    }
    if (butSquare) {
        butSquare.addEventListener("click", setAction("square"));
    }
    if (butSqrt) {
        butSqrt.addEventListener("click", setAction("squareRoot"));
    }
    if (butEqu) {
        butEqu.addEventListener("click", takeAction());
    }
    if (butClear) {
        butClear.addEventListener("click", clearCurr());
    }
    if (butDot) {
        butDot.addEventListener("click", setDot());
    }
}
function takeAction() {
    return function () {
        console.log('takeAction Called');
        if (action === null) {
            return;
        }
        if (action === "add") {
            add(num1, num2);
        }
        if (action === "sub") {
            sub(num1, num2);
        }
        if (action === "mul") {
            mul(num1, num2);
        }
        if (action === "div") {
            div(num1, num2);
        }
        if (action === "square") {
            square(num1);
        }
        if (action === "squareRoot") {
            squareRoot(num1);
        }
        if (results) {
            results.innerHTML = "<center><li>" + curr + "</li></center>";
        }
        num1 = curr;
        num2 = 0;
        dot = false;
        divisor = 10;
    };
}
