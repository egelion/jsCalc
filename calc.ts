//Evan Boren
let curr: number = 0;
let num1: number = 0;
let num2: number = 0;
/*var foo = function() { };
var bar = {};
var baz = { name: 'Eric', age: 35 };

baz.name;
baz['name'];

var foobar = [];
foobar.push({ name: 'Evan' });

foobar[0];
foobar.pop();
for (var i = 0; i < foobar.length; i++) {
	foobar[i];
}
*/
let action: string | null = null;
let results: HTMLUListElement | null = null;  //= document.getElementById("results").getElementsByTagName("li");
let dot: boolean = false;
let divisor = 10;

function setNum1(num: number){
	if(!dot){
		num1 = num1*10 + num;
	}
	else{
		num1 = num1 + (num/divisor);
		divisor = 10*divisor;
	}
	console.log('num1 updated');
	if(results){results.innerHTML = "<li>" + num1 + "</li>";}
}

function setNum2(num: number){
	if(!dot){
		num2 = num2*10 + num;
	}
	else{
		num2 = num2 + (num/divisor);
		divisor = 10*divisor;
	}
	console.log('num2 updated');
	if(results){results.innerHTML = "<li>" + num2 + "</li>";}
}

function setCurr(num: number){
	curr = num;
	console.log('curr updated');
	if(results){results.innerHTML = "<li>" + curr + "</li>";}
}

function numPress(num: number){
	console.log('Called numPress with: ' + num);
    let theNum: number = num;
	return function() {
        //console.log(theNum);
		//console.log(one);
		//console.log(two);
		//console.log(three);
		if     (action === null){setNum1(theNum);}
		else if(action !== null){setNum2(theNum);}
		console.log('numPress called');
	}
}

function add(x: number,y: number){
	let result: number = 0;
	result = (x + y);
	console.log('add called');
	setCurr(result);
}

function sub(x: number,y: number){
	let result: number = 0;
	result = (x - y);
	console.log('sub called');
	setCurr(result);
}

function mul(x: number,y: number){
	let result: number = 0;
	result = (x * y);
	console.log('mul called');
	setCurr(result);
}

function div(x: number,y: number){
	let result: number = 0;
	result = (x / y);
	console.log('div called');
	setCurr(result);
}

function clearCurr(){
	return function(){
		console.log('clearCurr called');
		setCurr(0);
		num1 = 0;
		num2 = 0;
		dot = false;
		if(results){results.innerHTML = "<center><li>" + curr + "</li></center>";}
		action = null;
	}
}

function square(num: number){
	console.log('square called');
	setCurr(num*num);
}

function squareRoot(num: number){
	console.log('squareRoot called');
	setCurr(Math.sqrt(num));
}

function setAction(x: string | null){

	let newAction: string | null = x;
	return function(){

		action = newAction;
		console.log('setAction Called');
		if(action === "square")	   {takeAction();}
		if(action === "squareRoot"){takeAction();}
		//if(action === "equ"){takeAction();}
		dot = false;
		divisor = 10;
	}
}

function setDot(){
	return function(){
		dot = true;
	}
}

function initialize(){
	curr = 0;
	num1 = 0;
	num2 = 0;
	action = null;
	divisor = 10;
	results = document.getElementById("results") as HTMLUListElement;
	let dot: boolean = false;
	let but1: HTMLElement | null = document.getElementById("but1");
	let but2: HTMLElement | null = document.getElementById("but2");
	let but3: HTMLElement | null = document.getElementById("but3");
	let but4: HTMLElement | null = document.getElementById("but4");
	let but5: HTMLElement | null = document.getElementById("but5");
	let but6: HTMLElement | null = document.getElementById("but6");
	let but7: HTMLElement | null = document.getElementById("but7");
	let but8: HTMLElement | null = document.getElementById("but8");
	let but9: HTMLElement | null = document.getElementById("but9");
	let but0: HTMLElement | null = document.getElementById("but0");
	let butAdd: HTMLElement | null = document.getElementById("butAdd");
	let butMin: HTMLElement | null = document.getElementById("butMin");
	let butDiv: HTMLElement | null = document.getElementById("butDiv");
	let butMul: HTMLElement | null = document.getElementById("butMul");
	let butSquare: HTMLElement | null = document.getElementById("butSquare");
	let butSqrt: HTMLElement | null = document.getElementById("butSqrt");
	let butEqu: HTMLElement | null = document.getElementById("butEqu");
	let butClear: HTMLElement | null = document.getElementById("butClear");
	let butDot: HTMLElement | null = document.getElementById("butDot");

	if(but1){but1.addEventListener("click",numPress(1));}
	if(but2){but2.addEventListener("click",numPress(2));}
	if(but3){but3.addEventListener("click",numPress(3));}
	if(but4){but4.addEventListener("click",numPress(4));}
	if(but5){but5.addEventListener("click",numPress(5));}
	if(but6){but6.addEventListener("click",numPress(6));}
	if(but7){but7.addEventListener("click",numPress(7));}
	if(but8){but8.addEventListener("click",numPress(8));}
	if(but9){but9.addEventListener("click",numPress(9));}
	if(but0){but0.addEventListener("click",numPress(0));}
	if(butAdd){butAdd.addEventListener("click",setAction("add"));}
	if(butMin){butMin.addEventListener("click",setAction("sub"));}
	if(butMul){butMul.addEventListener("click",setAction("mul"));}
	if(butDiv){butDiv.addEventListener("click",setAction("div"));}
	if(butSquare){butSquare.addEventListener("click",setAction("square"));}
	if(butSqrt){butSqrt.addEventListener("click",setAction("squareRoot"));}
	if(butEqu){butEqu.addEventListener("click",takeAction());}
	if(butClear){butClear.addEventListener("click",clearCurr());}
	if(butDot){butDot.addEventListener("click",setDot());}
}

function takeAction(){
	return function(){
		console.log('takeAction Called');
		if(action === null){return;}
		if(action === "add"){add(num1,num2);}
		if(action === "sub"){sub(num1,num2);}
		if(action === "mul"){mul(num1,num2);}
		if(action === "div"){div(num1,num2);}
		if(action === "square"){square(num1);}
		if(action === "squareRoot"){squareRoot(num1);}
		if(results){results.innerHTML = "<center><li>" + curr + "</li></center>";}
		num1 = curr;
		num2 = 0;
		dot = false;
		divisor = 10;
	}
}


